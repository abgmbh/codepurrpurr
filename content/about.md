+++ 
title = "About" 
date = "2023-06-1"
[ author ] 
    name = "codepurrpurr.io" 
+++

I'm an engineer who enjoys writing about topics that genuinely interest me, both in my professional life and personal endeavors. My wide array of subjects includes automation, authentication, cloud networking, and security, among others.

Fascinated by the allure of solutions that may appear deceptively simple but require intricate thinking and design, I have a keen interest in unraveling their inner workings.

I hold great admiration for practical approaches that effectively address problems while placing utmost importance on delivering the finest user experience.

Let me introduce you to the star behind the scenes, our 10-year-old cat named Latte. Whenever I dive into my code, Latte joins me, purring in approval and adding a touch of feline wisdom. Inspired by Latte's coding companionship, I proudly named the site codepurrpurr.io. Because when it comes to coding expertise, Latte's purrs speak volumes.

Our Git repository is here:

- <https://gitlab.com/abgmbh>
